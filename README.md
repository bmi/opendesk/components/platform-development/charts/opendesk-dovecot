<!--
SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
SPDX-License-Identifier: Apache-2.0
-->
# Sovereign Workplace Dovecot Helm Charts

This repository contains a Helm chart for deploying Dovecot to work with
Open-Xchange within the Sovereign Workplace.

## Prerequisites

Before you begin, ensure you have met the following requirements:

- Kubernetes 1.21+
- Helm 3.0.0+
- PV provisioner support in the underlying infrastructure

## Documentation

The documentation is placed in the README of the dovecot helm chart:

- [dovecot](charts/dovecot)

## License

This project uses the following license: Apache-2.0

## Copyright

Copyright © 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
