{{- /*
SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
SPDX-License-Identifier: Apache-2.0
*/}}
#!/bin/sh
USER=$1
cat << EOF | /usr/lib/dovecot/dovecot-lda -d $USER
From: postmaster@{{ .Values.dovecot.mailDomain }}
Subject: Willkommen

Message.

EOF
